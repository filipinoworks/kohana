var app = angular.module('app', []);



app.controller('thumbnailController', function($scope, $timeout) {
	$scope.thumbnails = [];
	$scope.togglePopup = true;
	$scope.newUploadProcessing = false;
	$scope.editable = [];

	$scope.getThumbnails = function() {
		$.ajax({
			url: "/thumbnail/list",  
			type: "POST",
			dataType: "json", 
			success: function(result){
	        	$scope.thumbnails = result;
	        	$scope.$apply();
	        	console.log($scope.thumbnails)

	    }});
	}
	$scope.togglePopupFunc = function() {
		$('#thumbnail_upload').val("");
		$scope.editable = [];
		$scope.error_message = "";
		$scope.togglePopup = !$scope.togglePopup;
		$scope.$apply();
	} 
	$scope.toggleConfirmModal = function($index) {
		$scope.to_delete_index = $index;
		$scope.toggleDeleteModal = !$scope.toggleDeleteModal;
	}
	$scope.saveNewThumbnail = function() {
		$scope.newUploadProcessing = true;
		var form = $('#newThumbnail')[0];
		var formData = new FormData(form);
		$.ajax({
	        url: '/thumbnail/save',
	        enctype: 'multipart/form-data',
	        type: 'post',
	        processData: false,
	        contentType: false,
	        data: formData,
	        success: function(data) {
	        	data = jQuery.parseJSON(data);
	        	if(data.success == false) {
	        		$scope.error_message = data.message;
	        		$scope.newUploadProcessing = false;
	        		$timeout( function(){
			            $scope.error_message = "";
			            $scope.$apply();
			        }, 4000 );

	        		$scope.$apply();
	        		return;
	        	}
	        	console.log(data)
	        	$scope.newUploadProcessing = false;
	        	$scope.togglePopup = !$scope.togglePopup;
	        	$scope.getThumbnails();
	        	$scope.$apply();
	        },
	        error: function(xhr, status, thrown) {
	        	console.log(xhr)
	        	$scope.newUploadProcessing = false;
	        	$scope.$apply();
	        }
	    });
	}

	$scope.editFile = function($index) {
		$('#thumbnail_upload').val("");
		$scope.togglePopup = !$scope.togglePopup;
		$scope.editable["file_name"] = $scope.thumbnails[$index]["file_name"];
		$scope.editable["thumb_id"] = $scope.thumbnails[$index]["thumb_id"];
		$scope.editable["title"]    = $scope.thumbnails[$index]["title"];
	}

	$scope.deleteThumbnail = function() {
		var postData ={'thumb_id':$scope.thumbnails[$scope.to_delete_index].thumb_id};
		$.ajax({
	        url: '/thumbnail/delete',
	        type: 'POST',
			dataType: "JSON",
            data:  postData,
	        success: function(data) {
	        	$scope.thumbnails.splice($scope.to_delete_index, 1);
	        	$scope.toggleDeleteModal = true;
	        	$scope.$apply();
	        },
	        error: function(xhr, status, thrown) {
	        	console.log(status)
	        	$scope.$apply();
	        }
	    });
	}

	$scope.getThumbnails();
});