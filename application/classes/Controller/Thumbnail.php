<?php defined('SYSPATH') or die('No direct script access.');


class Controller_Thumbnail extends Controller {

	private $thumbnail_model; 

	public function action_index()
	{
		$this->response->body(View::factory('list'));
	}

	public function action_list()
	{
		$this->thumbnail_model = ORM::factory('Thumbnail');
		$data = $this->thumbnail_model->get_thumb();

		foreach ($data as $key => $value) {
			$encrypt = Encrypt::instance('default');
			$encrypted_data = $encrypt->encode($data[$key]["thumb_id"]);
			$data[$key]["thumb_id"] = $encrypted_data;
			$data[$key]["date_added"] = date("D, d M Y", strtotime($data[$key]["date_added"]));
		}
		
		echo json_encode($data);
	}
	public function action_delete()
	{
		$decrypt = Encrypt::instance('default');
		$decrypted_data = $decrypt->decode($this->request->post('thumb_id'));
		$data["thumb_id"] = $decrypted_data;

		$data = array(
			"thumb_id"     => $data["thumb_id"]);

		$this->thumbnail_model = ORM::factory('Thumbnail');
		$this->thumbnail_model->deleteByThumbId($data);
		

	
		$previous_data = $this->thumbnail_model->find_thumb($decrypted_data);
		
		//deleting file
		$directory = DOCROOT.'storage/thumbnails/';
		if(file_exists($directory.$previous_data[0]["file_name"])) {
			unlink($directory.$previous_data[0]["file_name"]);
		}
		


		$message = array();
		$message["succss"] = true;

		echo json_encode($message);
	}
	public function action_save()
	{	
		$message = array();
		$width = 80;
		$height = 80;

		$this->thumbnail_model = ORM::factory('Thumbnail');

		if($_FILES["thumbnail"]["size"] > 0) {
			
			$files = Validation::factory($_FILES);
			$files->rule('thumbnail', 'Upload::type', array(':value', array('jpg', 'png')));

            // check input data
            if (!$files->check())
            {
            	$message["success"] = false;
            	$message["message"] = "Please upload an image";
            	echo json_encode($message);
            	die;
            	
            }	
			$directory = DOCROOT.'storage/thumbnails/';
			$image     = $_FILES["thumbnail"];
			$filename  = $_FILES['thumbnail']['name'];
			$data      = array();
			$count     = 1;

			if($this->request->post('thumb_id') != null || $this->request->post('thumb_id') != "") {
				$decrypt = Encrypt::instance('default');
				$decrypted_data = $decrypt->decode($this->request->post('thumb_id'));
				$previous_data = $this->thumbnail_model->find_thumb($decrypted_data);
				//deleting file
				if(file_exists($directory.$previous_data[0]["file_name"])) {
					unlink($directory.$previous_data[0]["file_name"]);
				}
				
			}

	 		while(file_exists($directory.$filename)) {
			    $count ++;
			    $filename = $count."_".$filename;
			}

	        if ($file = Upload::save($image, NULL, $directory))
	        {
	            Image::factory($file)
	                ->resize($width, $height, Image::AUTO)
	                ->save($directory.$filename);
	            unlink($file);
	            
	            $data["filename"] = $filename;
	            
	        }
	       
	        $data = array(
				"file_name"  => $filename,
	        );

		} else {
			if($this->request->post('thumb_id') == null ) {
            	$message["success"] = false;
            	$message["message"] = "Image is required.";
            	echo json_encode($message);
            	die;
			}
		}


        
		$date_added         = new DateTime();
		
		$data["date_added"] = $date_added->format('Y-m-d H:i:s');
		$data["title"]     = $this->request->post('title');
		//update
		if($this->request->post('thumb_id') != null || $this->request->post('thumb_id') != "") {

			$decrypt = Encrypt::instance('default');
			$decrypted_data = $decrypt->decode($this->request->post('thumb_id'));
			$data["thumb_id"] = $decrypted_data;
			$this->thumbnail_model->update($data);
		} else {
			$this->thumbnail_model->create($data);
		}
        

        $data["success"]    = true;

        echo json_encode($data);
	}
	

} 
