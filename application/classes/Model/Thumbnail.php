<?php

class Model_Thumbnail extends Model
{
    private $table;
  
    public function __construct()
    {
        $this->table = 'tbl_thumbnails';
    }
    public function get_thumb()
    {
        $data = DB::select('thumb_id','title', 'file_name' , 'date_added')
            ->from($this->table)
            ->where("is_deleted", "=" , "No")
            ->execute()->as_array();
        return $data;
    }
    public function find_thumb($id)
    {
        $data = DB::select('thumb_id','title', 'file_name' , 'date_added')
            ->from($this->table)
            ->where("thumb_id", "=" , $id)
            ->execute()->as_array();
        return $data;
    }
    public function create($data)
    {
        DB::insert( $this->table, array_keys($data) )
            ->values( array_values($data))
            ->execute();
    }
    public function deleteByThumbId($data)
    {
        DB::update($this->table)->set(array('is_deleted' => "Yes"))->where('thumb_id', '=', $data["thumb_id"])
            ->execute();
    } 
    public function update($data)
    {
        if(isset($data["file_name"])) {
            DB::update($this->table)->set(array('title' => $data["title"], "file_name" => $data["file_name"]))->where('thumb_id', '=', $data["thumb_id"])
            ->execute();
        } else {
            DB::update($this->table)->set(array('title' => $data["title"]))->where('thumb_id', '=', $data["thumb_id"])
            ->execute();
        }
        
    } 

      
     
}

?>