<!DOCTYPE html>
<html>
<head>
	<title>Thumbnail Lists</title>
	<?php
    	echo HTML::style('../assets/css/thumbnail.css');
    ?>
</head>
<body ng-app = "app" ng-controller = "thumbnailController">
		<div class="table-wrapper" ng-cloak >
			<div class="table-layout">
				<div class="btn-wrapper-row">
					<input type="text" name="" ng-model = "file_search" placeholder="search file">
					<button class="add-new-btn sm btn-flat-blue" ng-click = "togglePopupFunc();editable=[]">Add new</button>
				</div>
				<table>
					<thead>
						<th>Title</th>
						<th>Thumbnail</th>
						<th>File Name</th>
						<th>Date Added</th>
						<th>Edit</th>
						<th>Delete</th>
					</thead>
					<tbody>
						<tr ng-repeat = "thumbnail in thumbnails | filter : file_search track by $index">
							<td ng-cloak ng-bind = "thumbnail.title || '-'"></td>
							<td ng-cloak><img width = "50" height = "50" src="/storage/thumbnails/{{thumbnail.file_name}}"></td>
							<td ng-cloak ng-bind = "(thumbnail.file_name | limitTo: 10) + (thumbnail.file_name.length > 10 ? '...' : '')"></td>
							<td ng-bind = "thumbnail.date_added"></td>
							<td><button class="sm btn-flat-blue" ng-click = "editFile($index)">edit</button></td>
							<td><button class="sm btn-flat-danger" ng-click = "toggleConfirmModal($index)">delete</button></td>
						</tr>
						<tr ng-show = "thumbnails.length == 0"><td colspan="6">No records to show</td></tr>
					</tbody>
				</table>
			</div>	
		</div>

		<div id="thumbModal" class="modal hidden" ng-cloak  ng-class = "{hidden : togglePopup}" ng-init = "togglePopup = true">
			<div class="modal-content">
			    <span class="close" ng-click = "togglePopupFunc()">&times;</span>
			    <h3 ng-show = "editable['thumb_id'] ==  null">Add New Thumbnail</h3>
			    <h3 ng-show = "editable['thumb_id'] != null">Update Thumbnail</h3>
			    <form id = "newThumbnail" enctype="multipart/form-data">
				    <table>
				    	<tr>
				    		<td>Thumbnail</td>
				    		<td><input type="file" class="" id = "thumbnail_upload" name="thumbnail" accept="image/x-png, image/gif, image/jpeg, image/jpg" ></td>
				    	</tr>

				    	<tr>
				    		<td>Title</td>
				    		<td>
				    			<input type="hidden"  name="thumb_id" value = "{{editable['thumb_id']}}">
				    			<input type="text"  name="title" ng-model = "editable['title']">
				    		</td>
				    	</tr>
				    </table>
			    	<div class="btn-lower">
			    		<button class = "sm btn-flat btn-flat-blue sm" ng-disabled = "newUploadProcessing" ng-click = "saveNewThumbnail()">Save</button>
			    		<button class = "sm btn-flat btn-flat-danger sm" ng-disabled = "newUploadProcessing" ng-click = "togglePopupFunc()">Cancel</button>
				    	<img class="loader" ng-class = "{hidden : !newUploadProcessing}" src="../assets/images/loading.svg">
			    	</div>
			    	<div class="error-message error">
				    	{{error_message}}
				    </div>
			    </form>
			</div>
		</div>

		<div id="confirmModal" class="modal hidden" ng-class = "{hidden : toggleDeleteModal}" ng-init = "toggleDeleteModal = true">
			<div class="modal-content confirm-dialog">
			    <div class="header">
			    	<label>Are you sure you want to delete this?</label>
			    </div>
			    <div  class="btn-lower">
			    	<button class="btn-flat btn-flat-blue sm" ng-click = "deleteThumbnail($to_delete_index)">Yes</button>
			    	<button class="btn-flat btn-flat-danger sm" ng-click = "toggleDeleteModal = true">Cancel</button>
			    </div>

			</div>
		</div>

	</div>

	<?php echo html::script('../assets/js/angular.min.js'); ?>
	<?php echo html::script('../assets/js/jquery-3.2.1.min .js'); ?>
	<?php echo html::script('../assets/app/thumbnails/app.js'); ?>
</body>
</html>