
<?php defined('SYSPATH') or die('No direct script access.');
// application/config/encrypt.php
 
return array(
 
    'default' => array(
        'key'    => 'trwQwVXX96TIJoKxyBHB9AJkwAOHixuV1ENZmIWyanI0j1zNgSVvqywy044Agaj',
        'cipher' => MCRYPT_RIJNDAEL_128,
        'mode'   => MCRYPT_MODE_NOFB,
    ),

);